

// const vm = new Vue({
//     el: '#app',
//     data: {
//       results: [
//         {title: "the very first post", abstract: "lorem ipsum some test dimpsum"},
//         {title: "and then there was the second", abstract: "lorem ipsum some test dimsum"},
//         {title: "third time's a charm", abstract: "lorem ipsum some test dimsum"},
//         {title: "four the last time", abstract: "lorem ipsum some test dimsum"}
//       ]
//     }
//   });

const vm = new Vue({
    el: '#app',
    data: {
      results:[]
    },
    mounted() {
      axios.get("https://api.nytimes.com/svc/topstories/v2/home.json?api-key=9d665c4fcc1e4384a86bdf354904a3eb")
      .then(response => {this.results = response.data.results})
    }
  });